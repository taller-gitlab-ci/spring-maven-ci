package org.guzman.springmaven.controllers;

import lombok.RequiredArgsConstructor;
import org.guzman.springmaven.services.SumService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/sum")
public class SumController {

    private final SumService sumService;

    @GetMapping("")
    private Integer sumTwoNumber(@RequestParam(defaultValue = "0") Integer s1,
                                @RequestParam(defaultValue = "0") Integer s2) {
        return sumService.execute(s1, s2);
    }
}

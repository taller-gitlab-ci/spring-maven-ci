package org.guzman.springmaven.controllers;

import lombok.RequiredArgsConstructor;
import org.guzman.springmaven.services.SubtractionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/subtract")
public class SubtractionController {

    private final SubtractionService subtractionService;

    @GetMapping("")
    private Integer subtractTwoNumbers(
            @RequestParam(defaultValue = "0") Integer s1,
            @RequestParam(defaultValue = "0") Integer s2) {

        return subtractionService.execute(s1, s2);
    }
}

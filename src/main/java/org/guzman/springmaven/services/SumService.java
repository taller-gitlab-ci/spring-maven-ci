package org.guzman.springmaven.services;

import org.springframework.stereotype.Service;

@Service
public class SumService {
    public Integer execute(Integer s1, Integer s2) {
        return s1 + s2;
    }
}

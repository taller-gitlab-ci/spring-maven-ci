package org.guzman.springmaven.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumServiceTest {

    @Test
    void givenZeroValueAddends_thenZeroAsResult() {
        var s1 = 0;
        var s2 = 0;

        var sumService = new SumService();
        var res = sumService.execute(s1, s2);

        assertEquals(0, res);
    }

    @Test
    void givenNegativeValueAddends_thenNegativeAsResult() {
        var s1 = -2;
        var s2 = -3;

        var sumService = new SumService();
        var res = sumService.execute(s1, s2);

        assertEquals(-5, res);
    }

    @Test
    void givenPositiveValueAddends_thenPositiveAsResult() {
        var s1 = 2;
        var s2 = 3;

        var sumService = new SumService();
        var res = sumService.execute(s1, s2);

        assertEquals(5, res);
    }
}

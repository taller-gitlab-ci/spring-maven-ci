package org.guzman.springmaven.services;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubtractionServiceTest {

    @Test
    void givenZeroValueMinuendAndSubtrahend_thenZeroAsResult() {
        var minuend = 0;
        var subtrahend = 0;

        var subtractionService = new SubtractionService();
        var res = subtractionService.execute(minuend, subtrahend);

        assertEquals(0, res);
    }

    @Test
    void givenSameValues_thenZeroAsResult() {
        var minuend = 3;
        var subtrahend = 3;

        var subtractionService = new SubtractionService();
        var res = subtractionService.execute(minuend, subtrahend);

        assertEquals(0, res);
    }

    @Test
    void givenSameValuesDiffSigns_thenDoubleValueAsResult() {
        var minuend = 3;
        var subtrahend = -3;

        var subtractionService = new SubtractionService();
        var res = subtractionService.execute(minuend, subtrahend);

        assertEquals(6, res);
    }

    @Test
    void givenTwoValues_thenSubtractionOk() {
        var minuend = 10;
        var subtrahend = 3;

        var subtractionService = new SubtractionService();
        var res = subtractionService.execute(minuend, subtrahend);

        assertEquals(7, res);
    }
}
